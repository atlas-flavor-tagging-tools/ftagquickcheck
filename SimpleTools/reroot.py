# This script can convert the standard h5 output from TDD to a root file (flat n-tuple)
# The goal is to make the TDD output more accessible, so that people can quickly adopt their
# favorite ROOT macros or scripts taking root files as inputs. This should be treated as a standalone tool
# , and this is only a working example.
# Core functionalities are suggested and tested by Sam van Stroud (sam.vanstroud.18@ucl.ac.uk).
# Author: Bingxuan Liu (bingxuan.liu@cern.ch)
# Please keep in mind that you need uproot4

import h5py
import uproot
import numpy as np

jetVariables = {
  "taggers": [
    "GN120220509_pc",
    "GN120220509_pu",
    "GN120220509_pb",
    "DL1dv01_pu",
    "DL1dv01_pc",
    "DL1dv01_pb",
    "pt_btagJes",
    "eta_btagJes",
  ],
  "bhadrons": [
    "HadronConeExclTruthLabelPt",
    "HadronConeExclTruthLabelLxy",
    "GhostBHadronsFinalPt",
    "HadronConeExclTruthLabelID",
    "HadronConeExclExtendedTruthLabelID"
  ],
  "events": [
    "averageInteractionsPerCrossing",
    "actualInteractionsPerCrossing",
    "nPrimaryVertices",
  ]
}

trackVariables = {
  "hits": [
    "numberOfInnermostPixelLayerHits",
    "numberOfNextToInnermostPixelLayerHits",
    "numberOfInnermostPixelLayerSharedHits",
    "numberOfInnermostPixelLayerSplitHits",
    "numberOfPixelHits",
    "numberOfPixelHoles",
    "numberOfPixelSharedHits",
    "numberOfPixelSplitHits",
    "numberOfSCTHits",
    "numberOfSCTHoles",
    "numberOfSCTSharedHits",
    "expectNextToInnermostPixelLayerHit",
    "expectInnermostPixelLayerHit"
    ],
  "kinematics": [
    "qOverP",
    "chiSquared",
    "radiusOfFirstHit",
    "pt",
    "deta",
    "z0RelativeToBeamspot",
    "qOverPUncertainty"
  ],
  #Need to cast them to full precision
  "halves":[
    "d0",
    "z0SinTheta",
    "eta",
    "d0Uncertainty",
    "z0SinThetaUncertainty",
    "phiUncertainty",
    "thetaUncertainty",
    "dphi",
    "dr",
    "ptfrac",
    "numberDoF",
    "theta",
    "z0RelativeToBeamspotUncertainty",
    "IP3D_signed_d0",
    "IP2D_signed_d0",
    "IP3D_signed_z0",
    "IP3D_signed_d0_significance",
    "IP3D_signed_z0_significance"
   ]
}

# Here we set the total number of elements to obtain. If there are fewer in the file, it will not cause errors or warnings. 
stats = 300000
jetVariablesToInclude = ["taggers","bhadrons","events"]
trackVariablesToInclude = ["hits","kinematics"]
jetVariableList = []
trackVariableList = []

inputFile = h5py.File('user.pgadow.29879664._000002.output.h5')

for category in jetVariablesToInclude:
  jetVariableList = jetVariableList + jetVariables[category]

# One can check the content of an h5 file by doing h5ls
jets = inputFile['jets'].fields(jetVariableList)[:stats]

for category in trackVariablesToInclude:
  trackVariableList = trackVariableList + trackVariables[category]

tracks = inputFile['tracks_loose'].fields(trackVariableList)[:stats]

trackVariableArrays = {}

#Deal with halves
for variable in trackVariables["halves"]:
  variableArrayHalf = inputFile['tracks_loose'].fields([variable])[:stats]
  # Need to cast the half-precision variables to full
  variableArrayFull = np.array(variableArrayHalf, dtype=np.float32)
  trackVariableArrays["tracks_" + variable] = variableArrayFull

with uproot.recreate("run3.root") as rootFile:
    writeVariables = {"jets": jets, "tracks": tracks}
    for variable in trackVariableArrays:
      writeVariables[variable ] = trackVariableArrays[variable]
    rootFile["data"] = writeVariables                                  
