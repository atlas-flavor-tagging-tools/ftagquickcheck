CppQuickCheck
=============

This is a minimal example to access jets and b-tagging from an xAOD. A
lot of things are hardcoded for now, please read the code and
un-hardcode them!

Note also that this is set up to work with AthAnalysis, if you want to
use a different project it should be easy, just edit the top level
CMakeLists.txt file.

Quick Start
-----------

You have to set up `AthAnalysis` and then build this like any other
ATLAS project, e.g.

- Go to the parent directory of this project
- `asetup AthAnalysis,master,latest
- `mkdir build`
- `cd build`
- `cmake ../FTAGQuickCheck`
- `make`
- `. x86*/setup.sh`

Then to run, use `example path/to/AOD.root`.
