# FTAGQuickCheck

Some examples to perform quick checks on DAODs or the dumped h5 datasets

## Installing uproot4, needed by reroot.py

If you are running it locally, you can install uproot4 easily with either pip or brew

If you want to run this on lxplus, virtual environment is the best solution.

## Using the C++ code

See the README under CppQuickCheck.

## Using the plotting script, which takes root histograms as the inputs

python FTAGStack.py -l stackConfig.py

