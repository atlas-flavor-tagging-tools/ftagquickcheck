import os,sys,re
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-d','--directory', dest='directory', help='Path to the sample directory.',default="")
parser.add_argument('-n','--numberToSplit', dest='numberToSplit', help='Number of sub-directories to create.', default=1, type = int)
arguments = parser.parse_args()

sourceDir = arguments.directory
nDir = arguments.numberToSplit

# Caution if your samples have non-root formats
listFiles = os.popen("ls " + sourceDir + "/*.root").readlines()
nFiles = len(listFiles)
nFilesPerJob = int(nFiles/nDir)

for i in range(0, nDir):
    new_dir = sourceDir + "_" + str(i)
    os.system("mkdir " + new_dir)
    for j in range(nFilesPerJob*i, nFilesPerJob*(i+1)):
        os.system("mv " + listFiles[j].rstrip('\n') + " " + new_dir)

new_dir = sourceDir + "_" + str(nDir - 1)
for i in range(nFilesPerJob*nDir, nFiles):
    os.system("mv " + listFiles[i].rstrip('\n') + " " + new_dir)
